
console.log("Hello World");

let firstName = "John";
console.log("First Name:" + ' ' + firstName);

let lastName = "Smith";
console.log("Last Name:" + ' ' + lastName);

let age = "30";
console.log("Age:" + ' ' + age);

let hobbies = "Hobbies:";
console.log(hobbies);

let activity = ['Biking', 'Mountain Climbing', 'Swimming']
console.log(activity)

let workAddress = "Work Address:";
console.log(workAddress);

let address = {
	houseNumber: "32", street: "Washington", city: "Lincoln", state: "Nebraska"
};
console.log(address);

// Functions

function details() {
	console.log("John Smith is 30 years of age.");
};
details();

function printUserInfo(firstName, lastName, age, hobbies, address) {
			console.log(firstName + " " + lastName + " " + age)
		}

		printUserInfo("John", "Smith", "30");

		printUserInfo('Biking', 'Mountain Climbing', 'Swimming')

		printUserInfo("houseNumber: 32", "street: Washington", "city: Lincoln", "state: Nebraska")